#!/bin/sh

set -eu

cleanup() {
	rm -f tmp.md
}

trap cleanup EXIT

(
	cat vmdb2.md
	for x in vmdb/plugins/*.mdwn; do
		cat "$x"
		echo
	done
) >tmp.md
pandoc \
	--self-contained \
	--standalone \
	--css vmdb2.css \
	--toc \
	--number-sections \
	--metadata-file=vmdb2.subplot \
	-o vmdb2.html \
	tmp.md
