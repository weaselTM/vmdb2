# Copyright 2023  Andy Piper
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=

import json

import vmdb


class LvscanPlugin(vmdb.Plugin):
    def enable(self):
        self.app.step_runners.add(LvscanStepRunner())


class LvscanStepRunner(vmdb.StepRunnerInterface):
    def get_key_spec(self):
        return {"lvscan": str, "tags": []}

    def run(self, values, settings, state):
        for v in get_logical_volumes(values):
            dev = v["lv_path"]
            tag = v["lv_name"]
            vmdb.progress(f"remembering {dev} as {tag}")
            state.tags.append(tag)
            state.tags.set_dev(tag, dev)
            vmdb.runcmd(["lvchange", "-a", "y", dev])

    def teardown(self, values, settings, state):
        vgname = values["lvscan"]
        vmdb.runcmd(["vgchange", "-an", vgname])


def get_logical_volumes(values):
    vgname = values["lvscan"]
    tags = values["tags"]
    vmdb.runcmd(["lvscan"])
    volumes = json.loads(
        vmdb.runcmd(
            [
                "lvdisplay",
                "-C",
                "-o",
                "lv_name,vg_name,lv_path",
                "--report-format",
                "json",
            ]
        )
    )["report"][0]["lv"]

    # ensure all tags have a corresponding volume, but not all volumes need
    # a matching tag
    matched_vols = [
        v for v in volumes if v["lv_name"] in tags and v["vg_name"] == vgname
    ]
    if len(matched_vols) < len(tags):
        matched_tags = [v["lv_name"] for v in matched_vols]
        unmatched_tags = sorted(set(tags).difference(matched_tags))
        raise RuntimeError(
            f'unmatched tags in volume group "{vgname}": {unmatched_tags}'
        )
    return matched_vols
